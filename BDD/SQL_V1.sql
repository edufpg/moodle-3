DROP DATABASE  IF exists Moodle3;
create DATABASE Moodle3;

use Moodle3;
CREATE TABLE cursos(
    cursosId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255),
    asignaturas VARCHAR(255),
    alumnos VARCHAR(255)
);
CREATE TABLE profesores(
    profesoresId INT  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    nombreProfesor varchar(20),
    apellidosProfesor varchar(50),
    emailProfesor varchar(255),
    asignaturas VARCHAR(255)
);
CREATE TABLE aulas(
    aulasid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255),
    asignaturas VARCHAR(255)/*Lista relacionada de aulas*/
);
CREATE TABLE alumnos(
    alumnosId INT  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    nombreAlumno varchar(20),
    apellidosAlumno varchar(50),
    emailAlumno varchar(255),
    fNacAlumno VARCHAR(255),
    cursosId INT,
    profesoresId INT,
    notasId INT,
	CONSTRAINT cursosalumnos FOREIGN KEY (cursosId)
    REFERENCES cursos(cursosId),
    CONSTRAINT profesoresalumnos FOREIGN KEY (profesoresId)
    REFERENCES profesores(profesoresId)
    
);

CREATE TABLE usuarios(
	usuarioId INT  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    email varchar(255),
    username varchar(255),
    profesoresId INT,
    alumnosId INT,
    constraint usuariosprofesores foreign key (profesoresId)
    references profesores(profesoresId),
    constraint usuariosalumnos foreign key (alumnosId)
    references alumnos(alumnosId)
);

CREATE TABLE asignaturas(
	asignaturasId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(255),
    profesoresId INT,
    cursosId INT,
    CONSTRAINT asignaturasprofesores FOREIGN KEY (profesoresId)
    REFERENCES profesores(profesoresId),
    CONSTRAINT cursosasignaturas2 FOREIGN KEY (cursosId)
    REFERENCES cursos(cursosId)
    
);

CREATE TABLE notas(
	notasId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    asignaturasId INT,
    alumnosId Int,
    CONSTRAINT cursosasignaturas FOREIGN KEY (asignaturasId)
    REFERENCES asignaturas(asignaturasId),
    CONSTRAINT notasalumnos FOREIGN KEY (alumnosId)
    REFERENCES alumnos(alumnosId)
    
);

CREATE TABLE tareas(
	tareaId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    duracion INT,
    prioridad INT,
    alumnosId INT,
    puntuacion INT,
    CONSTRAINT tareasalumnos FOREIGN KEY (alumnosId)
    REFERENCES alumnos(alumnosId)
    
);

CREATE TABLE configuraciones(
	tipo INT,
    layout varchar(255),
    usuarioId INT,
    constraint usuariosconfig foreign key (usuarioId)
    references usuarios(usuarioId)

);


